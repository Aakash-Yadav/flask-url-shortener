from flask import Flask,render_template,request
import  pyshorteners

app = Flask(__name__)
@app.route('/')
def home():
    return render_template("shot.html") 
@app.route('/send',methods=['POST'])
def url(sum=sum):
    if request.method == 'POST':
        val = (request.form['URL'])
        if len(val) > 5:
            sho = pyshorteners.Shortener()
            sum = sho.tinyurl.short(val)   
            return render_template("shot.html",sum=sum)
        else:
            sum = False
            sum2 = 'Url Is To Short Try Again'
            return render_template("shot.html",sum2=sum2)
#https://www.google.com/url?sa=i&url=https%3A%2F%2Flucifer.fandom.com%2Fwiki%2FSeason_3&psig=AOvVaw1MxoGM4NmionwrCGgWe9PV&ust=1616085410165000&source=images&cd=vfe&ved=2ahUKEwjhpNW34bfvAhV5u2MGHe2NAk0QjRx6BAgAEAc

if __name__ == "__main__":
    app.run(debug=True)