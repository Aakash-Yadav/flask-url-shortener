# Flask Url Shortener

Using Flask and PyShortener module create web app 
### requirements module 
1. [pip3 install pyshorteners](https://pypi.or> g/project/pyshorteners/)
2. [pip3 install Flask](https://pypi.org/project/Flask/)


### Flask 
Flask is a web framework, it’s a Python module that lets you develop web applications easily. It’s has a small and easy-to-extend core: it’s a microframework that doesn’t include an ORM (Object Relational Manager) or such features.
It does have many cool features like url routing, template engine. It is a WSGI web app framework.

### pyshorteners
_pyshorteners is a Python lib to help you short and expand urls using the most famous URL Shorteners availables._
> [Documentation])(https://pyshorteners.readthedocs.io/en/latest/)

### Web-app 
- ##### Local Host: http://127.0.0.1:5000/
![alt text](https://xp.io/storage/1BNA0Yk0.png)

##### Copy The short Url by click Ok Button 








